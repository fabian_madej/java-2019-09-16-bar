package fabian.task;

import fabian.task.bar.Bar;
import fabian.task.bar.Barman;
import fabian.task.bar.Client;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bar bar = new Bar();
        Scanner input = new Scanner(System.in);

        Barman barman1 = new Barman(bar);
        Client c1 = new Client(bar,"Wojtek");
        Client c2 = new Client(bar,"Piotrek");
        Client c3 = new Client(bar,"Michal");
        Client c4 = new Client(bar,"Adam");
        Client c5 = new Client(bar,"Marek");

        Thread barman = new Thread(barman1);
        Thread client1 = new Thread(c1);
        Thread client2 = new Thread(c2);
        Thread client3 = new Thread(c3);
        Thread client4 = new Thread(c4);
        Thread client5 = new Thread(c5);

        client1.start();
        client2.start();
        client3.start();
        client4.start();
        client5.start();
        barman.start();

        input.next();
        {
            barman1.stop();
            c1.stop();
            c2.stop();
            c3.stop();
            c4.stop();
            c5.stop();
        }

    }
}
