package fabian.task.bar;

import java.util.Random;
import java.util.Scanner;

public class Barman implements Runnable{

    private String[] menu = new String[]{
            "Bloody Mary",
            "Margarita",
            "Old Fashioned Cocktail",
            "Mojito",
            "Daiquiri",
            "Gin and Tonic",
            "Screwdriver",
            "Gimlet"
    };
    private Bar bar = new Bar();

    private boolean end=true;
    private Random rand = new Random();
    public Barman(Bar bar) {
        this.bar = bar;
    }

    @Override
    public void run() {
        while (end){
            try {
                Thread.sleep(2000);
                String drink=menu[rand.nextInt(8)];
                bar.put_drink(drink);
                System.out.println("Barman put drink "+drink);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        end = false;
        Thread.interrupted();
    }
}
