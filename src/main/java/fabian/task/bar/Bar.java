package fabian.task.bar;

public class Bar {
    private String drink = null;
    public synchronized String take_drink() throws InterruptedException {
        while (drink == null) {
            wait();
        }
        String ret = drink;
        drink = null;
        return ret;
    }
    public synchronized void put_drink(String drink) {
        this.drink = drink;
        notifyAll();
    }
}
