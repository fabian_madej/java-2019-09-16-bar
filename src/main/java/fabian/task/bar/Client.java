package fabian.task.bar;

import java.util.Scanner;

public class Client implements Runnable{
    private String name;
    private Bar bar = new Bar();
    private boolean end=true;

    public Client(Bar bar, String name) {
        this.bar = bar;
        setName(name);
    }
    private void setName(String name){
        this.name=name;
    }
    private String getName(){
        return name;
    }

    @Override
    public void run() {
        while (end) {
            try {
                String drink = bar.take_drink();
                System.out.println(getName() + " drink " + drink);
                Thread.sleep(drink.length()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void stop(){
        end=false;
        Thread.interrupted();
    }
}
